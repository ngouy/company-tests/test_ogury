class CreateTweets < ActiveRecord::Migration[5.0]
  def change
    create_table :tweets, id: :uuid do |t|

      t.datetime :t_created_at, null: false
      t.string   :t_id,         null: false, uniq: true
      t.string   :t_user_id,    null: false
      t.string   :hashtags
      t.string   :text
      t.boolean  :truncated
      t.string   :source
      t.string   :lang

      t.timestamps
    end
  end
end
