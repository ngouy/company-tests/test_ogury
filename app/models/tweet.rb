class Tweet < ApplicationRecord

  def self.find_or_create!(tweet)
    Tweet.find_by_t_id(tweet.id) || Tweet.create_from_tweet!(tweet)
  end

  def self.create_from_tweet!(tweet)
    new_tweet = Tweet.new(
      t_created_at: tweet.created_at,
      t_id: tweet.id,
      t_user_id: tweet.user.id,
      hashtags: tweet.hashtags.map(&:text).join(' ')
    )
    [:text, :truncated ,:source, :lang].each {|attr| new_tweet[attr] = tweet.send attr }
    new_tweet.save!
  end

end
