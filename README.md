# README

This README would normally document whatever steps are necessary to get the
application up and running.

## to make the app run
- git clone this repo
- edit [/config/database.yml](./config/database.yml) with your own database settings
   >(running under posgresql)
- run `$ bundle install`
- run `$ rake db:migrate`
- run `$ rails s -p 4242`
- your app is now running at [http://localhost:4242/](http://localhost:4242/)

## how to use it

- go to [http://localhost:4242/:hashtag](http://localhost:4242/:hashtag) to retrives recents tweets with this hashtag
  > note that `hashtag` is case insensitive
- optinal params :
  - `number` : number of hashtag to retrieve (max: 100, default: 20)
  - `offline` : boolean to retrieve tweets in offline mode (thoose already retrive in online mode and registered in db) (default: false)

> <http://localhost:4242/tweets/gg><br>will retrieve last 20 online tweets with #gg hashtag
>
> <http://localhost:4242/tweets/gg?number=10&offline=true><br>will retrieve last 30 tweets which contains #gg hashtag in your db

## how to improve
- bulk insert tweets
- do not iterate on each tweet to know which one is already registered or not
- do a front page
- ...

time elapsed (out of this readme/publish, out of previously hangout call: 1h10)